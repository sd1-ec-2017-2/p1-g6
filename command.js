canal = require('./canal.js');
/**
*Este metod resolve os comandos IRC recebidos pelo servidor.
*
*@method resolveComandos
*@param {String} linha recebida do cliente via socket
*@param {Object} Array de objetos client
*@param {Object} Socket atual conectado ao servidor
*/
function resolveComandos(msg, clientes, socket, canais){
	var args = String(msg).split(" ");
	switch(String(args[0]).toUpperCase()){
		case 'PRIVMSG':
			return privmsg(args, clientes, socket);
		case 'NICK':
			return nick(args, clientes, socket);
		case 'USER':
			return user(args,socket);
		case 'OPER':
			return;
		case 'QUIT':
			return;
		case 'PASS':
			return password(args);
		case 'JOIN':
			return join(canais, args, socket, clientes);

		default:
			return 421;
	}
}
module.exports = resolveComandos;

/**
* função que envia mensagens broadcast
*@method broadcast
*@param {String} linha recebida do cliente via socket
*@param {Object} Array de objs clients
*@param {Object} Socket atual conectado ao servidor
*/
function broadcast(message, clientes, socketSender) {
	console.log(clientes);
    clientes.forEach(function (client) {
      // Don't want to send it to sender
      if (client.socket === socketSender) return;
      client.socket.write(message);
    });
    // Log it to the server output too
    process.stdout.write(message)
  }


//função qu everifica se o nickname já está cadastrado em algum cliente 
function isNicknameExistente(nickname, clientes){
		for (var i = 0; i < clientes.length; i++) {
			if(String(clientes[i].nickname) == nickname){
				return true;
			} 
		}
		return false;
	}

function isUserExistente(username, clientes){
	if(clientes!=null){
		for (var i = 0; i < clientes.length; i++) {
			if(String(clientes[i].username) == username){
				return true;
			} 
		}
		return false;}
	}

function isCanalExistente(canalStr, listaCanais){
	if(listaCanais!=null){
		for (var i = 0; i < listaCanais.length; i++) {
			if(String(listaCanais[i].nomeCanal) == canalStr){
				return true;
			} 
		}
		return false;}
	}




// AS FUNÇÕES QUE EFETIVAMENTE RESOLVE OS COMANDOS ABAIXO.

function nick(args, clientes, socket){
	if(args.length<2){
		return 431;
	//confirir expressão regular nickname   =  ( letter / special ) *8( letter / digit / special / "-" )
	}else if(! new RegExp('[A-Za-z\[\]\\`_\^\{\|\}][A-Za-z0-9\[\]\\`_\^\{\|\}\-]{0,8}'))	{// RESOLVER- QUANDO NICKNAME USA CARACTER NÃO PERMITIDO ++++++++++---+===============>>>>>>>>>>>
		return 432;
	}else if(isNicknameExistente(args[1], clientes)){
		return 433;
	}else {
		return null;
	}
}



/* função resolve o comando user e retorna um codigo numeric responses
*/
function user(args,socket,clientes){

	if(args.length<5/*461    ERR_NEEDMOREPARAMS  Não tem parametros suficientes*/){
		return 461;
	}else if(isUserExistente(args[1],clientes)/*462    ERR_ALREADYREGISTRED Comando não autorizado (Já registrado)*/){
		return 462;
	}
	else {
	return null;
	}
}
// resolve o comando pass
function password(args){
	if(args.length<2){
		return "ERR_NEEDMOREPARAMS\n";
	}else if(false){// RESOLVER- password ja registrada
		return "ERR_ALREADYREGISTRED\n"
	}else {
	var pass = args[1];}
	return "password sucessfull\n";
}
function oper(){
	if(false/*461    ERR_NEEDMOREPARAMS sem parametros suficiente*/){
		return 461;
	}else if(false/* 491    ERR_NOOPERHOST No O-lines for your host*/){
		return 491;
	}else if(false/*381    RPL_YOUREOPER  você é agora um operador irc*/){
		return 381;
	}else if(false/*464    ERR_PASSWDMISMATCH senha incorreta*/){
		return 464;
	}
	
}

//trata o comando de mensagens privada
function privmsg(args, clientes, socket){
	if(args.length<3){
		return 412;


	}else if(false/*411    ERR_NORECIPIENT não é possivel enviar para o canal*/){
		return 411;
	}else if(false/*414	 ERR_WILDTOPLEVEL  Wildcard in toplevel domain */){
		return 414;
	}else if(! isNicknameExistente(args[1], clientes)){//falta construir as questoes de ca
		return 401;/*401	 ERR_NOSUCHNICK nenhum nickname ou canal cadastrado*/
	}else if(false/*301 	 RPL_AWAY o destinatario da mensagem está ausente está ausente*/){
		return 301;
	}else if(false/*413 	 ERR_NOTOPLEVEL: Nenhum domínio toplevel especificado"*/){
		return 413;
	}else if(false/*407 	 ERR_TOOMANYTARGETS*/){
		return 407;
	}else if(isNicknameExistente(args[1], clientes)){ // TUDO OK PODE ENVIAR A MENSAGEM
		for (var i = 0; i < clientes.length; i++) {
			if (String(clientes[i].nickname).trim()==String(args[1]).trim()) {
				var msg=args.slice(2).join(" ");
				clientes[i].socket.write(msg);
				break;
			}
		}
		return;
	}else{
		socket.write(" nickname não exite!")
	}
}

function join(canais, args, socket, clientes){
	console.log(canais)
	var canaisCMD=args[1].split(",");
	var chavesCMD=[];
	if(args.length>2){
		chavesCMD=args[2].split(",");
	}
	for(var i=chavesCMD.length;i<canaisCMD.length;i++){
		chavesCMD.push(null);
		
	}
	
	if(args.length<2/*461    ERR_NEEDMOREPARAMS sem parametros suficientes*/){
		return 461;
	}else if(false/*473    ERR_INVITEONLYCHAN não pode se juntar ao canal (+i)*/){
		return 473;
	}else if(false/*471    ERR_CHANNELISFULL não pode se juntar ao canal (+l)*/){
		return 471;
	
	}else if(false/*403    ERR_NOSUCHCHANNEL nenhum canal desse tipo*/){
		return 403;
	
	}else if(false/* 407    ERR_TOOMANYTARGETS*/){
		return 407;
	
	}else if(false/*332    RPL_TOPIC retornando o topico do canal ao cliente que requisitou */){
		return 332;	
	}else if(false/*474    ERR_BANNEDFROMCHAN não pode se juntar ao canal (+b)*/){
		return 474;
	}else if(false/*475    ERR_BADCHANNELKEY não pode se juntar ao canal (+k)*/){
		return;
	}else if(false/*476    ERR_BADCHANMASK mascara de canal ruim (fora do conformes)*/){
		return 476;
	}else if(false/*405    ERR_TOOMANYCHANNELS você se juntou a muitos canais */){
		return 405;
	}else if(false/*437    ERR_UNAVAILRESOURCE nick/channel está temporariamente indisponivel */){
		return;
	}else{
		var clienteTemp=null;
		for(var j=0;j<canaisCMD.length;j++){
			if(!isCanalExistente(canaisCMD[j], canais)){
				var cn=new canal(canaisCMD[j], chavesCMD[j]);
				for(var i=0;i<clientes.length;i++){
					if(clientes[i].socket.name==socket.name){
						clienteTemp=clientes[i];
						break;
					}
				}

			cn.channelClients.push(clienteTemp)
			canais.push(cn);
		}else{
            		for (var aux = 0; aux < canais.length; aux++) {
			if(String(canais[aux].nomeCanal) == canaisCMD[j]){
				break;
			} 
}
            if(canais[aux].senha==chavesCMD[j]){
			for(var i=0;i<clientes.length;i++){
					if(clientes[i].socket.name==socket.name){
						clienteTemp=clientes[i];
						break;
				    	}
					}
				}
				
			//canal existe
		}
		}
	}
}