// Load the TCP Library
net = require('net');
client=require('./client.js');
comandos = require('./command.js');
numReplies = require('./numericReplies.json');
// Keep track of the chat clients
var clients = [];
var clientesServ = [];
var canais = [];
var qtdclientes=0;



// Start a TCP Server
net.createServer(function (socket) {
  var cliente = new client("", socket);
  // Identify this client
  socket.name = socket.remoteAddress + ":" + socket.remotePort 
 
  // Put this new client in the list
  clients.push(socket);

  // Send a nice welcome message and announce
  socket.write("Welcome " + socket.name + "\n");
  broadcast(socket.name + " joined the chat\n", socket);
  var mensagem;
  // Handle incoming messages from clients.
  var ultimoComando;
  var nicknameTemp ;
  var usernameTemp ;
  var nomerealTemp;
  var modeTemp;
  socket.on('data', function (data) {
  

  //Envia para resolução de mensagem	
	mensagem = String(data).trim();
   	var responses = comandos(mensagem, clientesServ, socket, canais);

   	if (mensagem.split(" ")[0]=="NICK" || mensagem.split(" ")[0]=="nick") {nicknameTemp=mensagem.split(" ")[1]}
   	if (mensagem.split(" ")[0]=="USER"|| mensagem.split(" ")[0]=="user") {usernameTemp=mensagem.split(" ")[1];
                                                                          modeTemp=mensagem.split(" ")[2];
                                                                           nomerealTemp=mensagem.split(":")[1];}
                                         
   	if(responses!=null){
  		for(key in numReplies){
        if(key==responses){
				  socket.write(numReplies[key]+"\n");
				  break;
			 }
		  }
    }else if(mensagem.split(" ")[0]=="USER"|| mensagem.split(" ")[0]=="user"){
		  var cliente = new client(nicknameTemp,usernameTemp, socket,nomerealTemp,modeTemp);
		  clientesServ.push(cliente);
      qtdclientes++;
		  // mensagens para conectar com o chatzilla
  		socket.write("localhost:127.0.0.1 001 "+clientesServ[qtdclientes-1].nickname+
                   " : Welcome to the Internet Relay Chat Network "+clientesServ[qtdclientes-1].nickname+"!~chatzilla@h200137216145.ufg.br\n");
	}
			//verificar quando cadastrar conexão
			
	ultimoComando=mensagem.split(" ")[0];
  });

  // Remove the client from the list when it leaves
  socket.on('end', function () {
    clients.splice(clients.indexOf(socket), 1);
    broadcast(socket.name + " left the chat.\n");
  });
  
  // Send a message to all clients
  function broadcast(message, sender) {
    clients.forEach(function (client) {
      // Don't want to send it to sender
      if (client === sender) return;
      client.write(message);
    });
    // Log it to the server output too
    process.stdout.write(message)
  }

}).listen(6667);

// Put a friendly message on the terminal of the server.
console.log("Chat server running at port 6667\n");